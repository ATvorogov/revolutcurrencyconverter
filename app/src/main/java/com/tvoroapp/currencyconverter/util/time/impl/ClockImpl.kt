package com.tvoroapp.currencyconverter.util.time.impl

import com.tvoroapp.currencyconverter.util.time.Clock
import javax.inject.Inject

class ClockImpl @Inject constructor() : Clock {
    override fun currentTimeMillis() = System.currentTimeMillis()

}