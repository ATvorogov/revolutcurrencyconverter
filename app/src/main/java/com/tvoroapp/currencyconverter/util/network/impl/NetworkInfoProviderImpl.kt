package com.tvoroapp.currencyconverter.util.network.impl

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import com.tvoroapp.currencyconverter.util.network.NetworkInfoProvider
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import javax.inject.Inject

class NetworkInfoProviderImpl
@Inject constructor(private val application: Application) :
    NetworkInfoProvider {
    private val connManager
        get() = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    override val isNetworkConnected: Observable<Boolean> by lazy {
        val checkConnected: Unit.() -> Boolean = {
            connManager.activeNetworkInfo?.isConnected ?: false
        }
        val networkChangeFlow: Observable<Unit> = Observable.create<Unit> { e ->
            e.onNext(Unit)
            val receiver = NetworkChangeReceiver(e)
            val intentFilter = IntentFilter().apply { addAction(ConnectivityManager.CONNECTIVITY_ACTION) }
            application.registerReceiver(receiver, intentFilter)
            e.setCancellable { application.unregisterReceiver(receiver) }

        }
        return@lazy networkChangeFlow
            .map(checkConnected)
            .replay(1)
            .refCount()
    }
}

private class NetworkChangeReceiver(val rxEmitter: ObservableEmitter<Unit>) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        rxEmitter.onNext(Unit)
    }

}