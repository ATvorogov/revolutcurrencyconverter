package com.tvoroapp.currencyconverter.util.time

interface Clock {
    fun currentTimeMillis() : Long
}