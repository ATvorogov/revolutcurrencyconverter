package com.tvoroapp.currencyconverter.util.network

import io.reactivex.Observable

interface NetworkInfoProvider {
    val isNetworkConnected: Observable<Boolean>
}