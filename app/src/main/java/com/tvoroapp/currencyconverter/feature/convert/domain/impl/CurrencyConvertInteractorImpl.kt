package com.tvoroapp.currencyconverter.feature.convert.domain.impl

import com.tvoroapp.currencyconverter.feature.convert.domain.*
import com.tvoroapp.currencyconverter.util.network.NetworkInfoProvider
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.concurrent.TimeUnit
import javax.inject.Inject

const val REPEAT_TIMEOUT = 1L
const val DEFAULT_CURRENCY_VALUE = 1L

class CurrencyConvertInteractorImpl
@Inject constructor(
    private val currencyRateRepository: CurrencyRateRepository,
    private val networkInfoProvider: NetworkInfoProvider
) : CurrencyConvertInteractor {

    override fun observeNetworkState(): Observable<NetworkState> {
        return networkInfoProvider.isNetworkConnected.map {
            if (it) {
                NetworkState.CONNECTED
            } else {
                NetworkState.DISCONNECTED
            }
        }
    }

    override fun currencyConvertState(
        currencyCode: CurrencyCode,
        currencyAmount: Flowable<BigDecimal>
    ): Flowable<CurrencyCovertState> {
        val ratesFlow = createRatesEntityFlow(currencyCode)
        val amountFlow = currencyAmount.observeOn(Schedulers.computation())
        return Flowable.combineLatest(ratesFlow, amountFlow, BiFunction { rateEntity, amount ->
            createState(rateEntity, amount)
        })
    }

    override fun initialState(): Single<CurrencyCovertState> {
        return createInitialRatesEntityFlow()
            .observeOn(Schedulers.computation())
            .map { createState(it, BigDecimal(DEFAULT_CURRENCY_VALUE)) }
    }

    private fun createInitialRatesEntityFlow(): Single<CurrencyRateEntity> {
        val queryFromServer = Single.defer {
            networkInfoProvider.isNetworkConnected
                .switchMapSingle { isConnected ->
                    if (isConnected) {
                        currencyRateRepository
                            .queryInitialRates()
                            .retryWhen { a -> a.flatMap { Flowable.timer(REPEAT_TIMEOUT, TimeUnit.SECONDS) } }
                    } else {
                        Single.never()
                    }
                }
                .filter { entity -> entity.rates.isNotEmpty() }
                .firstOrError()
        }
        val cachedRates = currencyRateRepository.getLatestCachedRates()
        return cachedRates.switchIfEmpty(queryFromServer)
    }

    private fun createRatesEntityFlow(base: CurrencyCode): Flowable<CurrencyRateEntity> {
        val networkIsConnected = networkInfoProvider.isNetworkConnected.toFlowable(BackpressureStrategy.LATEST)
        val loadFromNetwork = networkIsConnected.switchMap { isConnected ->
            if (isConnected) {
                currencyRateRepository
                    .queryRates(base)
                    .repeatWhen { a -> a.flatMap { Flowable.timer(REPEAT_TIMEOUT, TimeUnit.SECONDS) } }
                    .retryWhen { a -> a.flatMap { Flowable.timer(REPEAT_TIMEOUT, TimeUnit.SECONDS) } }

            } else {
                Flowable.empty()
            }
        }
        /* Add delay before network call to prevent too closest emissions */
        val loadFromNetworkWithDelay = loadFromNetwork.delaySubscription(1L, TimeUnit.SECONDS)
        return currencyRateRepository.getCachedRates(base)
            .toFlowable()
            .concatWith(loadFromNetworkWithDelay)
    }

    private fun createState(rateEntity: CurrencyRateEntity, baseValue: BigDecimal): CurrencyCovertState {
        val convertedRawSequence = rateEntity.rates
            .asSequence()
            .filter { entry -> entry.key != rateEntity.baseCurrency }
            .map { entry ->
                val v = baseValue.multiply(BigDecimal(entry.value)).setScale(2, RoundingMode.HALF_DOWN)
                CurrencyRaw(entry.key, v, false)
            }
            .sortedBy { raw -> raw.currencyCode }
        val baseRawElement = CurrencyRaw(rateEntity.baseCurrency, baseValue, true)
        val resultList = sequenceOf(baseRawElement).plus(convertedRawSequence).toList()
        return CurrencyCovertState(rateEntity.loadingTime, resultList)
    }
}