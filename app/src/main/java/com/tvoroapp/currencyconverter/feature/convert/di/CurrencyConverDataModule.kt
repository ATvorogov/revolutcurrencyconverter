package com.tvoroapp.currencyconverter.feature.convert.di

import android.app.Application
import androidx.room.Room
import com.tvoroapp.currencyconverter.feature.convert.data.api.CurrencyApi
import com.tvoroapp.currencyconverter.feature.convert.data.cache.CurrencyRateDatabase
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class CurrencyConverDataModule {

    @Provides
    @Singleton
    fun provideRevolutRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://revolut.duckdns.org")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }

    @Provides
    fun provideCurrencyService(retrofit: Retrofit): CurrencyApi {
        return retrofit.create(CurrencyApi::class.java)
    }

    @Provides
    fun provideDatabase(application: Application): CurrencyRateDatabase {
        return Room.databaseBuilder(
            application,
            CurrencyRateDatabase::class.java,
            "CurrencyDatabase"
        ).build()
    }

    @Provides
    fun provideCurrencyDao(db: CurrencyRateDatabase) = db.currencyRateDao()

}