package com.tvoroapp.currencyconverter.feature.convert.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.mynameismidori.currencypicker.ExtendedCurrency.getCurrencyByISO
import com.tvoroapp.currencyconverter.R
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyRaw

class CurrencyViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup,
    private val vm: CurrencyConvertViewModel
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.item_currency, parent, false)) {

    private val image: ImageView = itemView.findViewById(R.id.image)
    private val currencyCode: TextView = itemView.findViewById(R.id.currency_code)
    private val currencyLabel: TextView = itemView.findViewById(R.id.currency_label)
    private val amountEdit: EditText = itemView.findViewById(R.id.amount)
    private var currentData: CurrencyRaw? = null

    init {
        amountEdit.doAfterTextChanged {
            if (currentData?.isBase == true) {
                vm.changeCurrencyAmount(it.toString())
            }
            itemView.setOnClickListener {
                if (currentData?.isBase == false) {
                    vm.selectBaseCurrency(currentData!!)
                }
            }
            amountEdit.setOnClickListener {
                if (currentData?.isBase == false) {
                    vm.selectBaseCurrency(currentData!!)
                    prepareAmountViewToEdit()
                }
            }
        }
    }

    fun bind(newData: CurrencyRaw) {
        val rawChanged = currentData?.currencyCode != newData.currencyCode
        if (rawChanged) {
            currencyCode.text = newData.currencyCode
            val extended = getCurrencyByISO(newData.currencyCode)?.apply { loadFlagByCode(itemView.context) }
            if (extended != null) {
                currencyLabel.text = extended.name
                image.setImageResource(extended.flag)
            }

        }
        currentData = newData

        amountEdit.isEnabled = newData.isBase
        amountEdit.isClickable = newData.isBase
        if (newData.isBase && !amountEdit.isFocused) {
            prepareAmountViewToEdit()
        }

        val newAmountText = newData.amount.toPlainString()
        if (!newData.isBase || rawChanged) {
            amountEdit.setText(newAmountText)
        }
    }

    private fun prepareAmountViewToEdit() {
        amountEdit.requestFocus()
        amountEdit.setSelection(amountEdit.text.length)
    }
}

