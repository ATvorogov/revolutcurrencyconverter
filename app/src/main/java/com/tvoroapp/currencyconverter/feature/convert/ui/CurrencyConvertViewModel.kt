package com.tvoroapp.currencyconverter.feature.convert.ui

import androidx.annotation.UiThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.tvoroapp.currencyconverter.feature.convert.domain.*
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val INITAL_LOAD_TIMEOUT = 500L
const val USER_INPUT_THROTTLE = 300L

class CurrencyConvertViewModel
@Inject constructor(private val interactor: CurrencyConvertInteractor) : ViewModel() {

    private val _convertListState = MediatorLiveData<CurrencyCovertState>()

    private var currencyCode: CurrencyCode? = null
    private var currentLoader: LiveData<CurrencyCovertState>
    private lateinit var currencyAmount: Subject<BigDecimal>

    val convertListState: LiveData<CurrencyCovertState> = _convertListState
    val networkState: LiveData<NetworkState> by lazy {
        val flow = interactor.observeNetworkState().toFlowable(BackpressureStrategy.LATEST)
        LiveDataReactiveStreams.fromPublisher(flow)
    }

    init {
        currentLoader = LiveDataReactiveStreams.fromPublisher(interactor.initialState().toFlowable())
        _convertListState.addSource(currentLoader) { data ->
            _convertListState.value = data
            val firstRaw = data.currencies.first()
            invalidateBaseCurrency(firstRaw.currencyCode, firstRaw.amount)
        }
    }

    @UiThread
    private fun invalidateBaseCurrency(code: CurrencyCode, amount: BigDecimal) {
        _convertListState.removeSource(currentLoader)
        currencyCode = code
        currencyAmount = BehaviorSubject.createDefault(amount)
        val currencyAmountFlow = currencyAmount.toFlowable(BackpressureStrategy.LATEST)
            .throttleLatest(USER_INPUT_THROTTLE, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
        val normalState = interactor.currencyConvertState(
            code,
            currencyAmountFlow
        )
        val baseRaw = CurrencyRaw(code, amount, true)
        val initialAndThenNormalState = initialStateFlow(baseRaw).delaySubscription(
            INITAL_LOAD_TIMEOUT,
            TimeUnit.MILLISECONDS,
            Schedulers.computation()
        ).concatWith(normalState)
        currentLoader = LiveDataReactiveStreams.fromPublisher(
            normalState.ambWith(initialAndThenNormalState)
        )
        _convertListState.addSource(currentLoader) { state -> _convertListState.value = state }
    }

    @UiThread
    fun changeCurrencyAmount(stringValue: String) {
        try {
            val amount = BigDecimal(stringValue)
            currencyAmount.onNext(amount)
        } catch (ignore: Exception) {
            // Ignore such errors. Later possible to pass such errors to crash analytics.
        }
    }

    @UiThread
    fun selectBaseCurrency(selectedRaw: CurrencyRaw) {
        val amount = if (selectedRaw.amount == BigDecimal.ZERO) {
            BigDecimal.ONE
        } else {
            selectedRaw.amount
        }
        invalidateBaseCurrency(selectedRaw.currencyCode, amount)
    }

    @UiThread
    private fun initialStateFlow(selectedRaw: CurrencyRaw): Flowable<CurrencyCovertState> {
        val currentData = _convertListState.value
        if (currentData != null) {
            return Flowable.fromCallable<CurrencyCovertState> {
                val amount = selectedRaw.amount
                val tailSequence = currentData.currencies.asSequence()
                    .filter { raw -> raw.currencyCode != selectedRaw.currencyCode }
                    .map { raw -> raw.copy(amount = BigDecimal.ZERO, isBase = false) }
                    .sortedBy { raw -> raw.currencyCode }
                val head = sequenceOf(selectedRaw.copy(isBase = true, amount = amount))
                return@fromCallable CurrencyCovertState(
                    0,
                    head.plus(tailSequence).toList()
                )
            }
        } else {
            return Flowable.empty()
        }
    }
}
