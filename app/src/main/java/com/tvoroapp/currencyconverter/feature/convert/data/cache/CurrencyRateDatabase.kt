package com.tvoroapp.currencyconverter.feature.convert.data.cache

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [CurrencyRate::class],
    version = 1
)
abstract class CurrencyRateDatabase : RoomDatabase() {

    abstract fun currencyRateDao(): CurrencyRateDao
}