package com.tvoroapp.currencyconverter.feature.convert.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.tvoroapp.currencyconverter.R
import com.tvoroapp.currencyconverter.core.di.Injector
import com.tvoroapp.currencyconverter.feature.convert.domain.NetworkState

class CurrencyConvertFragment : Fragment() {

    private lateinit var vm: CurrencyConvertViewModel
    private var connectionStateSnackbar: Snackbar? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        val factory = Injector.appComponent.vmFactory
        vm = ViewModelProviders.of(this, factory).get(CurrencyConvertViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_currency_converter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /* Setup Recycler view */
        val recycler = view.findViewById<RecyclerView>(R.id.currency_recycler)
        val adapter = CurrencyConvertAdapter(vm)
        val o = object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                if (fromPosition == 0 || toPosition == 0) {
                    recycler.scrollToPosition(0)
                }
            }
        }
        adapter.registerAdapterDataObserver(o)
        recycler.layoutManager = LinearLayoutManager(requireContext())
        recycler.setHasFixedSize(true)
        recycler.adapter = adapter
        recycler.setRecycledViewPool(RecyclerView.RecycledViewPool().also { it.setMaxRecycledViews(0, 10) })

        recycler.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))

        /* Bind view model */
        vm.convertListState.observe(viewLifecycleOwner, Observer { state ->
            adapter.submitList(state.currencies)
        })
        vm.networkState.observe(viewLifecycleOwner, Observer { state ->
            when (state!!) {
                NetworkState.CONNECTED -> {
                    removeSnackbar()
                }
                NetworkState.DISCONNECTED -> {
                    if (connectionStateSnackbar == null) {
                        connectionStateSnackbar = Snackbar.make(
                            view,
                            R.string.wait_network_connection,
                            Snackbar.LENGTH_INDEFINITE
                        )
                        connectionStateSnackbar?.show()
                    }
                }
            }
        })
    }

    private fun removeSnackbar() {
        connectionStateSnackbar?.dismiss()
        connectionStateSnackbar = null
    }
}