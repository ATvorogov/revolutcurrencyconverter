package com.tvoroapp.currencyconverter.feature.convert.domain

typealias CurrencyCode = String
typealias RateMultiplier = Double

data class CurrencyRateEntity(
    val baseCurrency: CurrencyCode,
    val loadingTime: Long,
    val rates: Map<CurrencyCode, RateMultiplier>
)