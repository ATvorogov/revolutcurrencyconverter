package com.tvoroapp.currencyconverter.feature.convert.domain

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import java.math.BigDecimal


enum class NetworkState {
    CONNECTED,
    DISCONNECTED
}

data class CurrencyRaw(
    val currencyCode: CurrencyCode,
    val amount: BigDecimal,
    val isBase: Boolean
)

data class CurrencyCovertState(
    val timestamp: Long,
    val currencies: List<CurrencyRaw>
)

interface CurrencyConvertInteractor {

    fun currencyConvertState(
        currencyCode: CurrencyCode,
        currencyAmount: Flowable<BigDecimal>
    ): Flowable<CurrencyCovertState>

    fun observeNetworkState(): Observable<NetworkState>

    fun initialState(): Single<CurrencyCovertState>
}