package com.tvoroapp.currencyconverter.feature.convert.di

import androidx.lifecycle.ViewModel
import com.tvoroapp.currencyconverter.core.di.ViewModelKey
import com.tvoroapp.currencyconverter.feature.convert.data.impl.CurrencyRateRepositoryImpl
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyConvertInteractor
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyRateRepository
import com.tvoroapp.currencyconverter.feature.convert.domain.impl.CurrencyConvertInteractorImpl
import com.tvoroapp.currencyconverter.feature.convert.ui.CurrencyConvertViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module(includes = [CurrencyConverDataModule::class])
abstract class CurrencyConverFeatureModule {
    @Binds
    abstract fun bindRepository(impl: CurrencyRateRepositoryImpl): CurrencyRateRepository

    @Binds
    abstract fun bindInteractor(impl: CurrencyConvertInteractorImpl): CurrencyConvertInteractor

    @Binds
    @IntoMap
    @ViewModelKey(CurrencyConvertViewModel::class)
    abstract fun bindCurrencyConvertVm(impl: CurrencyConvertViewModel): ViewModel

}