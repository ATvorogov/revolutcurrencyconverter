package com.tvoroapp.currencyconverter.feature.convert.data.api

import com.squareup.moshi.JsonClass
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

@JsonClass(generateAdapter = true)
data class CurrencyRatesResponse(
    val base: String,
    val rates: Map<String, Double>
)

interface CurrencyApi {

    @GET("/latest")
    fun loadRates(@Query("base") baseCurrency: String): Single<CurrencyRatesResponse>

    @GET("/latest")
    fun loadRates(): Single<CurrencyRatesResponse>
}