package com.tvoroapp.currencyconverter.feature.convert.domain

import io.reactivex.Maybe
import io.reactivex.Single

interface CurrencyRateRepository {

    fun queryRates(base: CurrencyCode): Single<CurrencyRateEntity>

    fun queryInitialRates(): Single<CurrencyRateEntity>

    fun getLatestCachedRates(): Maybe<CurrencyRateEntity>

    fun getCachedRates(base: CurrencyCode): Maybe<CurrencyRateEntity>
}