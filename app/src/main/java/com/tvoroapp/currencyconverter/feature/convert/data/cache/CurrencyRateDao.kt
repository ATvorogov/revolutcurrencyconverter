package com.tvoroapp.currencyconverter.feature.convert.data.cache

import androidx.annotation.WorkerThread
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Maybe

@Dao
abstract class CurrencyRateDao {

    @Query("SELECT * FROM CurrencyRate WHERE baseCurrency = :baseCurrency")
    abstract fun queryRates(baseCurrency: String): Maybe<CurrencyRate>

    @Query("SELECT * FROM CurrencyRate ORDER BY timestamp DESC")
    abstract fun queryLatestRates(): Maybe<CurrencyRate>

    @WorkerThread
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertRates(value: CurrencyRate)

}
