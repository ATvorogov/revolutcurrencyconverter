package com.tvoroapp.currencyconverter.feature.convert.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyRaw

const val AMOUNT_CHANGED = 1 shl 1
const val IS_BASE_CHANGED = 1 shl 2

class CurrencyConvertAdapter(private val vm: CurrencyConvertViewModel) :
    ListAdapter<CurrencyRaw, CurrencyViewHolder>(CurrencyCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        return CurrencyViewHolder(LayoutInflater.from(parent.context), parent, vm)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int, payloads: MutableList<Any>) {
        val item = getItem(position)
        holder.bind(item)
    }
}

class CurrencyCallback : DiffUtil.ItemCallback<CurrencyRaw>() {

    override fun areItemsTheSame(oldItem: CurrencyRaw, newItem: CurrencyRaw): Boolean {
        return oldItem.currencyCode == newItem.currencyCode
    }

    override fun areContentsTheSame(oldItem: CurrencyRaw, newItem: CurrencyRaw): Boolean {
        return oldItem == newItem
    }

    override fun getChangePayload(oldItem: CurrencyRaw, newItem: CurrencyRaw): Any? {
        return Unit
    }
}