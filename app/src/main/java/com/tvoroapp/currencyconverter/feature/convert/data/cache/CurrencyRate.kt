package com.tvoroapp.currencyconverter.feature.convert.data.cache

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

@Entity
@TypeConverters(
    value = [RatesConverter::class]
)
data class CurrencyRate(
    @PrimaryKey val baseCurrency: String,
    val timestamp: Long,
    val rates: Map<String, Double>
)

class RatesConverter {
    @TypeConverter
    fun fromString(value: String): Map<String, Double> {
        val type = Types.newParameterizedType(Map::class.java, String::class.java, Double::class.javaObjectType)
        val adapter = Moshi.Builder().build().adapter<Map<String, Double>>(type)
        return adapter.fromJson(value)!!
    }

    @TypeConverter
    fun fromStringMap(map: Map<String, Double>): String {
        val type = Types.newParameterizedType(Map::class.java, String::class.java, Double::class.javaObjectType)
        val adapter = Moshi.Builder().build().adapter<Map<String, Double>>(type)
        return adapter.toJson(map)!!
    }
}