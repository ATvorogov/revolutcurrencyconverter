package com.tvoroapp.currencyconverter.feature.convert.data.impl

import com.tvoroapp.currencyconverter.feature.convert.data.api.CurrencyApi
import com.tvoroapp.currencyconverter.feature.convert.data.api.CurrencyRatesResponse
import com.tvoroapp.currencyconverter.feature.convert.data.cache.CurrencyRate
import com.tvoroapp.currencyconverter.feature.convert.data.cache.CurrencyRateDao
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyCode
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyRateEntity
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyRateRepository
import com.tvoroapp.currencyconverter.util.time.Clock
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CurrencyRateRepositoryImpl
@Inject constructor(
    private val currencyApi: CurrencyApi,
    private val currencyRateDao: CurrencyRateDao,
    private val clock: Clock
) : CurrencyRateRepository {
    override fun getLatestCachedRates(): Maybe<CurrencyRateEntity> = currencyRateDao
        .queryLatestRates()
        .map { daoObject -> createEntity(daoObject) }
        .subscribeOn(Schedulers.io())

    override fun queryRates(base: CurrencyCode) = currencyApi
        .loadRates(base)
        .map { response -> createEntity(response) }
        .doOnSuccess { entity ->
            val daoObject = createDaoObject(entity)
            currencyRateDao.insertRates(daoObject)
        }

    override fun queryInitialRates() = currencyApi
        .loadRates()
        .map { response -> createEntity(response) }
        .doOnSuccess { entity ->
            val daoObject = createDaoObject(entity)
            currencyRateDao.insertRates(daoObject)
        }

    override fun getCachedRates(base: CurrencyCode): Maybe<CurrencyRateEntity> {
        return currencyRateDao
            .queryRates(base)
            .map { daoObject -> createEntity(daoObject) }
            .subscribeOn(Schedulers.io())
    }

    private fun createEntity(response: CurrencyRatesResponse): CurrencyRateEntity {
        return CurrencyRateEntity(
            response.base,
            clock.currentTimeMillis(),
            response.rates
        )
    }

    private fun createEntity(daoObject: CurrencyRate): CurrencyRateEntity {
        return CurrencyRateEntity(
            daoObject.baseCurrency,
            daoObject.timestamp,
            daoObject.rates
        )
    }

    private fun createDaoObject(entity: CurrencyRateEntity): CurrencyRate {
        return CurrencyRate(
            entity.baseCurrency,
            entity.loadingTime,
            entity.rates
        )
    }
}