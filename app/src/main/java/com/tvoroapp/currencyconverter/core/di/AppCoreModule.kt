package com.tvoroapp.currencyconverter.core.di

import android.app.Application
import com.tvoroapp.currencyconverter.core.App
import dagger.Module
import dagger.Provides

@Module
class AppCoreModule(private val app: App) {
    @Provides
    fun provideApplication(): Application = app
}