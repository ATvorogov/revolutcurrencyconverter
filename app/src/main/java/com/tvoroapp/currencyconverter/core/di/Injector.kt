package com.tvoroapp.currencyconverter.core.di

object Injector {
    lateinit var appComponent: AppComponent

    fun initialize(appComponent: AppComponent) {
        Injector.appComponent = appComponent
    }
}

