package com.tvoroapp.currencyconverter.core.di

import com.tvoroapp.currencyconverter.util.network.NetworkInfoProvider
import com.tvoroapp.currencyconverter.util.network.impl.NetworkInfoProviderImpl
import com.tvoroapp.currencyconverter.util.time.Clock
import com.tvoroapp.currencyconverter.util.time.impl.ClockImpl
import dagger.Binds
import dagger.Module

@Module
abstract class UtilsModule {

    @Binds
    abstract fun bindClock(impl: ClockImpl): Clock

    @Binds
    abstract fun bindNetworkProvider(impl: NetworkInfoProviderImpl): NetworkInfoProvider
}