package com.tvoroapp.currencyconverter.core.di

import androidx.lifecycle.ViewModelProvider
import com.tvoroapp.currencyconverter.feature.convert.di.CurrencyConverFeatureModule
import dagger.Component
import javax.inject.Singleton


@Component(
    modules = [
        AppCoreModule::class,
        UiInjectModule::class,
        UtilsModule::class,
        CurrencyConverFeatureModule::class
    ]
)
@Singleton
interface AppComponent {
    val vmFactory: ViewModelProvider.Factory
}