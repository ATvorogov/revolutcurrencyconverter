package com.tvoroapp.currencyconverter.core

import android.app.Application
import com.tvoroapp.currencyconverter.core.di.AppCoreModule
import com.tvoroapp.currencyconverter.core.di.DaggerAppComponent
import com.tvoroapp.currencyconverter.core.di.Injector

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        /* Initialize DI */
        val appComponent = DaggerAppComponent.builder()
            .appCoreModule(AppCoreModule(this))
            .build()
        Injector.initialize(appComponent)
    }
}