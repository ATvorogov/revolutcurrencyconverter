package com.tvoroapp.currencyconverter.util

import com.tvoroapp.currencyconverter.util.time.Clock


class FixedClock : Clock {
    var time: Long = 0L

    override fun currentTimeMillis(): Long = time
}