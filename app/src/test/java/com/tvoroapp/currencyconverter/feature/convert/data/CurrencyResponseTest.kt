package com.tvoroapp.currencyconverter.feature.convert.data

import com.squareup.moshi.Moshi
import com.tvoroapp.currencyconverter.feature.convert.data.api.CurrencyRatesResponse
import org.junit.Assert.assertEquals
import org.junit.Test

class CurrencyResponseTest {

    @Test
    fun testParsingCorrectness() {
        val answer = """
            {"base":"EUR",
            "date":"2018-09-06",
            "rates":{"AUD":1.622,"BGN":1.9626}}"""
        val expectedModel = CurrencyRatesResponse(
            "EUR",
            mapOf(
                "AUD" to 1.622,
                "BGN" to 1.9626
            )
        )
        val moshi = Moshi.Builder().build()
        val actual = moshi.adapter<CurrencyRatesResponse>(CurrencyRatesResponse::class.java).fromJson(answer)
        assertEquals(expectedModel, actual)
    }
}
