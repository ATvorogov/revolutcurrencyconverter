package com.tvoroapp.currencyconverter.feature.convert.domain

import com.tvoroapp.currencyconverter.feature.convert.domain.impl.CurrencyConvertInteractorImpl
import com.tvoroapp.currencyconverter.util.TestSchedulerRule
import com.tvoroapp.currencyconverter.util.network.NetworkInfoProvider
import io.mockk.MockKAnnotations
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import io.reactivex.BackpressureStrategy
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.concurrent.TimeUnit

class CurrencyConvertInteractorTest {
    @get: Rule
    val testSchedulerRule = TestSchedulerRule()

    @MockK
    lateinit var repository: CurrencyRateRepository

    @MockK
    lateinit var networkInfoProvider: NetworkInfoProvider

    lateinit var interactor: CurrencyConvertInteractorImpl

    @Before
    fun before() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        interactor = CurrencyConvertInteractorImpl(repository, networkInfoProvider)
    }

    @Test
    fun observeConnectedNetworkTest() {
        every {
            networkInfoProvider.isNetworkConnected
        }.answers {
            Observable.just(true)
        }
        val testObserver = interactor.observeNetworkState().test()
        testObserver.assertValue(NetworkState.CONNECTED)
    }

    @Test
    fun initialRatesWithCachePresent() {
        val rateEntity = CurrencyRateEntity(
            "EUR",
            1L,
            mapOf(
                "BAD" to 0.5,
                "AUD" to 2.0
            )
        )
        every {
            repository.getLatestCachedRates()
        }.answers {
            Maybe.just(rateEntity)
        }

        val testObserver = interactor.initialState().test()
        testSchedulerRule.triggerActions()

        val expected = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("EUR", BigDecimal(1.0), true),
                CurrencyRaw("AUD", BigDecimal(2.0).setScale(2, RoundingMode.HALF_DOWN), false),
                CurrencyRaw("BAD", BigDecimal(0.5).setScale(2, RoundingMode.HALF_DOWN), false)
            )
        )
        testObserver.assertValue(expected)
    }

    @Test
    fun initialRatesFromServerWithInternet() {
        val rateEntity = CurrencyRateEntity(
            "EUR",
            1L,
            mapOf(
                "BAD" to 0.5,
                "AUD" to 2.0
            )
        )
        every { repository.getLatestCachedRates() }.answers { Maybe.empty() }
        every { repository.queryInitialRates() } answers { Single.just(rateEntity) }
        every { networkInfoProvider.isNetworkConnected }.answers { Observable.just(true) }
        val testObserver = interactor.initialState().test()
        testSchedulerRule.triggerActions()

        val expected = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("EUR", BigDecimal(1.0), true),
                CurrencyRaw("AUD", BigDecimal(2.0).setScale(2, RoundingMode.HALF_DOWN), false),
                CurrencyRaw("BAD", BigDecimal(0.5).setScale(2, RoundingMode.HALF_DOWN), false)
            )
        )
        testObserver.assertValue(expected)
    }

    @Test
    fun initialRatesFromServerWithoutInternet() {

        every { repository.getLatestCachedRates() }.answers { Maybe.empty() }
        every { networkInfoProvider.isNetworkConnected }.answers { Observable.just(false) }
        val testObserver = interactor.initialState().test()
        testSchedulerRule.triggerActions()

        testObserver.assertNoValues()
        verify { repository.getLatestCachedRates() }
        confirmVerified(repository)
    }

    @Test
    fun getRatesWithoutInternet() {
        val cachedRates = CurrencyRateEntity(
            "EUR",
            1L,
            mapOf(
                "BAD" to 0.5,
                "AUD" to 2.0
            )
        )
        val amountFlow = BehaviorSubject.createDefault(BigDecimal(1.0)).toFlowable(BackpressureStrategy.LATEST)
        every { networkInfoProvider.isNetworkConnected }.answers { Observable.just(false) }
        every { repository.getCachedRates("EUR") }.answers { Maybe.just(cachedRates) }
        val testObserver = interactor.currencyConvertState("EUR", amountFlow).test()
        testSchedulerRule.triggerActions()

        val expected = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("EUR", BigDecimal(1.0), true),
                CurrencyRaw("AUD", BigDecimal(2.0).setScale(2, RoundingMode.HALF_DOWN), false),
                CurrencyRaw("BAD", BigDecimal(0.5).setScale(2, RoundingMode.HALF_DOWN), false)
            )
        )
        testObserver.assertValue(expected)
    }

    @Test
    fun getRatesWithInternet() {
        val cachedRates = CurrencyRateEntity(
            "EUR",
            1L,
            mapOf(
                "BAD" to 0.5,
                "AUD" to 2.0
            )
        )
        val serverRates = CurrencyRateEntity(
            "EUR",
            2L,
            mapOf(
                "BAD" to 2.0,
                "AUD" to 3.0
            )
        )

        val amountFlow = BehaviorSubject.createDefault(BigDecimal(1.0)).toFlowable(BackpressureStrategy.LATEST)
        every { networkInfoProvider.isNetworkConnected }.answers { Observable.just(true) }
        every { repository.getCachedRates("EUR") }.answers { Maybe.just(cachedRates) }
        every { repository.queryRates("EUR") }.answers { Single.just(serverRates) }
        val testObserver = interactor.currencyConvertState("EUR", amountFlow).test()
        testSchedulerRule.triggerActions()

        val cachedBasedExpected = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("EUR", BigDecimal(1.0), true),
                CurrencyRaw("AUD", BigDecimal(2.0).setScale(2, RoundingMode.HALF_DOWN), false),
                CurrencyRaw("BAD", BigDecimal(0.5).setScale(2, RoundingMode.HALF_DOWN), false)
            )
        )
        testObserver.assertValue(cachedBasedExpected)

        // Advance time by 1 second
        testSchedulerRule.advanceTimeBy(1, TimeUnit.SECONDS)
        val serverBasedExpected = CurrencyCovertState(
            2L,
            listOf(
                CurrencyRaw("EUR", BigDecimal(1.0), true),
                CurrencyRaw("AUD", BigDecimal(3.0).setScale(2, RoundingMode.HALF_DOWN), false),
                CurrencyRaw("BAD", BigDecimal(2.0).setScale(2, RoundingMode.HALF_DOWN), false)
            )
        )
        testObserver.assertValues(cachedBasedExpected, serverBasedExpected)

        // Advance time by 1 second
        testSchedulerRule.advanceTimeBy(1, TimeUnit.SECONDS)
        testObserver.assertValues(cachedBasedExpected, serverBasedExpected, serverBasedExpected)
    }
}