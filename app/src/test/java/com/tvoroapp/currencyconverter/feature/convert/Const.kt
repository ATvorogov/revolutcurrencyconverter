package com.tvoroapp.currencyconverter.feature.convert

import com.tvoroapp.currencyconverter.feature.convert.data.api.CurrencyRatesResponse
import com.tvoroapp.currencyconverter.feature.convert.data.cache.CurrencyRate
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyRateEntity

object CurrencyTestFactory {
    val timestamp = 1L
    val currencyCode = "EUR"
    val rateResponse = CurrencyRatesResponse("EUR", mapOf("AUD" to 0.5))
    val rateRoomObject = CurrencyRate(
        "EUR",
        1L,
        mapOf("AUD" to 0.5)
    )

    val rateEntity = CurrencyRateEntity(
        "EUR",
        1L,
        mapOf("AUD" to 0.5)
    )
}