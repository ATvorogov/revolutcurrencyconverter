package com.tvoroapp.currencyconverter.feature.convert.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyConvertInteractor
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyCovertState
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyRaw
import com.tvoroapp.currencyconverter.feature.convert.domain.NetworkState
import com.tvoroapp.currencyconverter.util.TestSchedulerRule
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.slot
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.concurrent.TimeUnit

class CurrencyConvertViewModelTest {

    @get: Rule
    val testSchedulerRule = TestSchedulerRule()
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @MockK
    lateinit var interactor: CurrencyConvertInteractor

    @Before
    fun before() {
        MockKAnnotations.init(this)
    }

    @Test
    fun testInitialLoadWhenCreate() {
        val initialState = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("EUR", BigDecimal(1.0), true),
                CurrencyRaw("AUD", BigDecimal(2.0).setScale(2, RoundingMode.HALF_DOWN), false)
            )
        )
        every { interactor.initialState() }.answers { Single.just(initialState) }
        every { interactor.currencyConvertState(any(), any()) }.answers { Flowable.never() }

        val viewModel = CurrencyConvertViewModel(interactor)
        assertEquals("value should not appeared before subscription", null, viewModel.convertListState.value)

        viewModel.convertListState.observeForever { }
        testSchedulerRule.triggerActions()

        assertEquals(initialState, viewModel.convertListState.value)
    }

    @Test
    fun loadCurrencyWithoutInitialValueTest() {
        val eurBaseState = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("EUR", BigDecimal(1.0), true),
                CurrencyRaw("BAD", BigDecimal(0.5).setScale(2, RoundingMode.HALF_DOWN), false)
            )
        )
        val initialState = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("EUR", BigDecimal(1.33), true)
            )
        )
        every { interactor.initialState() }.answers { Single.just(initialState) }
        every { interactor.currencyConvertState("EUR", any()) }.answers { Flowable.just(eurBaseState) }

        val viewModel = CurrencyConvertViewModel(interactor)
        viewModel.convertListState.observeForever { }
        testSchedulerRule.triggerActions()

        assertEquals(eurBaseState, viewModel.convertListState.value)
    }

    @Test
    fun selectOtherCurrencyWithoutTimeout() {
        val initialState = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("EUR", BigDecimal(1.0), true),
                CurrencyRaw("AUD", BigDecimal(2.0), false)
            )
        )
        val audBasedState = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("AUD", BigDecimal(2.0).setScale(2, RoundingMode.HALF_DOWN), true),
                CurrencyRaw("EUR", BigDecimal(1.0), false),
                CurrencyRaw("BAD", BigDecimal(0.5).setScale(2, RoundingMode.HALF_DOWN), false)
            )
        )
        every { interactor.initialState() }.answers { Single.just(initialState) }
        every { interactor.currencyConvertState("EUR", any()) }.answers { Flowable.empty() }
        every { interactor.currencyConvertState("AUD", any()) }.answers {
            Flowable.just(audBasedState)
        }

        /* Load initial state */
        val viewModel = CurrencyConvertViewModel(interactor)
        viewModel.convertListState.observeForever { }
        testSchedulerRule.triggerActions()

        /* Select new currency */
        viewModel.selectBaseCurrency(CurrencyRaw("AUD", BigDecimal(2.0), true))
        testSchedulerRule.triggerActions()
        assertEquals(audBasedState, viewModel.convertListState.value)
    }

    @Test
    fun selectOtherCurrencyWithTimeout() {
        val initialState = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("EUR", BigDecimal(1.0), true),
                CurrencyRaw("AUD", BigDecimal(2.0), false)
            )
        )
        val audBasedState = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("AUD", BigDecimal(2.0).setScale(2, RoundingMode.HALF_DOWN), true),
                CurrencyRaw("EUR", BigDecimal(1.0), false),
                CurrencyRaw("BAD", BigDecimal(0.5).setScale(2, RoundingMode.HALF_DOWN), false)
            )
        )
        every { interactor.initialState() }.answers { Single.just(initialState) }
        every { interactor.currencyConvertState("EUR", any()) }.answers { Flowable.empty() }
        every { interactor.currencyConvertState("AUD", any()) }.answers {
            Flowable.just(audBasedState).delay(501, TimeUnit.MILLISECONDS)
        }

        /* Load initial state */
        val viewModel = CurrencyConvertViewModel(interactor)
        viewModel.convertListState.observeForever { }
        testSchedulerRule.triggerActions()

        /* Select new currency and advance time by 0.5sec*/
        viewModel.selectBaseCurrency(CurrencyRaw("AUD", BigDecimal(2.0), true))
        testSchedulerRule.advanceTimeBy(500, TimeUnit.MILLISECONDS)
        val initialExpectedState = CurrencyCovertState(
            0L,
            listOf(
                CurrencyRaw("AUD", BigDecimal(2.0), true),
                CurrencyRaw("EUR", BigDecimal(0.0), false)
            )
        )
        assertEquals(initialExpectedState, viewModel.convertListState.value)

        /* Advance time by 501 sec and check, if load finished */
        testSchedulerRule.advanceTimeBy(501, TimeUnit.MILLISECONDS)
        assertEquals(audBasedState, viewModel.convertListState.value)
    }

    @Test
    fun testChangeAmount() {
        val initialState = CurrencyCovertState(
            1L,
            listOf(
                CurrencyRaw("EUR", BigDecimal(1.5), true)
            )
        )
        val slot = slot<Flowable<BigDecimal>>()
        every { interactor.initialState() }.answers { Single.just(initialState) }
        every { interactor.currencyConvertState("EUR", capture(slot)) }.answers { Flowable.never() }

        val viewModel = CurrencyConvertViewModel(interactor)
        viewModel.convertListState.observeForever { }
        testSchedulerRule.triggerActions()
        val testObserver = slot.captured.test()
        viewModel.changeCurrencyAmount("2.0")
        viewModel.changeCurrencyAmount("invalidValue")

        testObserver.assertValues(BigDecimal(1.5))

        /* Simulate user input throttle timeout */
        testSchedulerRule.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        testObserver.assertValues(BigDecimal(1.5), BigDecimal("2.0"))
    }

    @Test
    fun subscribeToNetworkState() {

        every { interactor.initialState() }.answers { Single.never() }
        every { interactor.observeNetworkState() }.answers { Observable.just(NetworkState.CONNECTED) }

        val viewModel = CurrencyConvertViewModel(interactor)
        viewModel.networkState.observeForever { }
        testSchedulerRule.triggerActions()

        assertEquals(NetworkState.CONNECTED, viewModel.networkState.value)
    }
}