package com.tvoroapp.currencyconverter.feature.convert.data

import com.tvoroapp.currencyconverter.feature.convert.CurrencyTestFactory
import com.tvoroapp.currencyconverter.feature.convert.data.api.CurrencyApi
import com.tvoroapp.currencyconverter.feature.convert.data.cache.CurrencyRateDao
import com.tvoroapp.currencyconverter.feature.convert.data.impl.CurrencyRateRepositoryImpl
import com.tvoroapp.currencyconverter.feature.convert.domain.CurrencyRateRepository
import com.tvoroapp.currencyconverter.util.TestSchedulerRule
import com.tvoroapp.currencyconverter.util.time.Clock
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.reactivex.Maybe
import io.reactivex.Single.error
import io.reactivex.Single.just
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException

class CurrencyRateRepositoryTest {

    @get: Rule
    val testSchedulerRule = TestSchedulerRule()

    @MockK
    lateinit var currencyApi: CurrencyApi
    @MockK
    lateinit var currencyRateDao: CurrencyRateDao
    @MockK
    lateinit var clock: Clock
    lateinit var repository: CurrencyRateRepository

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        repository = CurrencyRateRepositoryImpl(currencyApi, currencyRateDao, clock)
    }

    @Test
    fun queryRatesWithErrorTest() {
        val e = IOException()
        every { currencyApi.loadRates(any()) }.answers { error(e) }
        repository.queryRates("CODE")
            .test()
            .assertError(e)
    }

    @Test
    fun queryRatesWithSuccessTest() {
        val eur = "EUR"
        every { currencyApi.loadRates(eur) }.answers { just(CurrencyTestFactory.rateResponse) }
        every { clock.currentTimeMillis() }.answers { CurrencyTestFactory.timestamp }

        repository.queryRates(eur)
            .test()
            .assertValue(CurrencyTestFactory.rateEntity)
    }

    @Test
    fun getFromCacheTest() {
        every {
            currencyRateDao.queryRates(CurrencyTestFactory.currencyCode)
        }.answers {
            Maybe.just(CurrencyTestFactory.rateRoomObject)
        }

        val testObserver = repository.getCachedRates(CurrencyTestFactory.currencyCode).test()
        testSchedulerRule.triggerActions()
        testObserver.assertValue(CurrencyTestFactory.rateEntity)
    }

    @Test
    fun getLatestFromCacheTest() {
        every { currencyRateDao.queryLatestRates() }.answers { Maybe.just(CurrencyTestFactory.rateRoomObject) }

        val testObserver = repository.getLatestCachedRates().test()
        testSchedulerRule.triggerActions()
        testObserver.assertValue(CurrencyTestFactory.rateEntity)
    }

    @Test
    fun queryInitialRatesTest() {

        every {
            currencyApi.loadRates(CurrencyTestFactory.currencyCode)
        }.answers {
            just(CurrencyTestFactory.rateResponse)
        }
        every { clock.currentTimeMillis() }.answers { CurrencyTestFactory.timestamp }

        repository.queryRates(CurrencyTestFactory.currencyCode)
            .test()
            .assertValue(CurrencyTestFactory.rateEntity)
    }
}